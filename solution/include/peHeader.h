/// @file
/// @brief PE read structure
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/// Structure File Header
struct
#ifdef _MSC_VER
#pragma packed(push, 1)
#else // _MSC_VER
	__attribute__((packed))
#endif
	PEHeader
{
	/// @name File Header
	///@{

	///The number that identifies the type of target machine. For more information, see Machine Types.
	uint16_t Machine;
	///The number of sections. This indicates the size of the section table, which immediately follows the headers.
	uint16_t NumberOfSections;
	///The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
	uint32_t TimeDateStamp;
	///The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
	uint32_t PointerToSymbolTable;
	///The number of entries in the symbol table.
	uint32_t NumberOfSymbols;
	///The size of the optional header, which is required for executable files but not for object files.
	uint16_t SizeOfOptionalHeader;
	///The flags that indicate the attributes of the file. For specific flag values, see Characteristics.
	uint16_t Characteristics;
	///@}
};

///Structure Optional header
struct
#ifdef _MSC_VER
#pragma packed(push, 1)
#else // _MSC_VER
	__attribute__((packed))
#endif
	OptionalHeader
{
	/// @name Optional Header
	///@{

	///The unsigned integer that identifies the state of the image file.
	uint16_t Magic;
	///The linker major version number.
	uint8_t  MajorLinkerVersion;
	///The linker minor version number.
	uint8_t  MinorLinkerVersion;
	///The size of the code (text) section, or the sum of all code sections if there are multiple sections.
	uint32_t SizeOfCode;
	///The size of the initialized data section, or the sum of all such sections if there are multiple data sections.
	uint32_t SizeOfInitializedData;
	///The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.
	uint32_t SizeOfUninitializedData;
	///The address of the entry point relative to the image base when the executable file is loaded into memory.
	uint32_t AddressOfEntryPoint;
	///The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
	uint32_t BaseOfCode;
	///The preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K.
	uint64_t ImageBase;
	///The alignment (in bytes) of sections when they are loaded into memory.
	uint32_t SectionAlignment;
	///The alignment factor (in bytes) that is used to align the raw data of sections in the image file.
	uint32_t FileAlignment;
	///The major version number of the required operating system.
	uint16_t MajorOperatingSystemVersion;
	///The minor version number of the required operating system.
	uint16_t MinorOperatingSystemVersion;
	///The major version number of the image.
	uint16_t MajorImageVersion;
	///The minor version number of the image.
	uint16_t MinorImageVersion;
	///The major version number of the subsystem.
	uint16_t MajorSubsystemVersion;
	///The minor version number of the subsystem.
	uint16_t MinorSubsystemVersion;
	///Reserved, must be zero.
	uint32_t Win32VersionValue;
	///The size (in bytes) of the image, including all headers, as the image is loaded in memory.
	uint32_t SizeOfImage;
	///The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.
	uint32_t SizeOfHeaders;
	///The image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL.
	uint32_t CheckSum;
	///The subsystem that is required to run this image.
	uint16_t Subsystem;
	///For more information, see DLL Characteristics later in this specification.
	uint16_t DllCharacteristics;
	///The size of the stack to reserve.
	uint64_t SizeOfStackReserve;
	///The size of the stack to commit.
	uint64_t SizeOfStackCommit;
	///The size of the local heap space to reserve.
	uint64_t SizeOfHeapReserve;
	///The size of the local heap space to commit.
	uint64_t SizeOfHeapCommit;
	///Reserved, must be zero.
	uint32_t LoaderFlags;
	///The number of data-directory entries in the remainder of the optional header.
	uint32_t NumberOfRvaAndSizes;
	///data directory.
	uint64_t DataDirectory[16];
	///@}
};

/// Structure Section header
struct
#ifdef _MSC_VER
#pragma packed(push, 1)
#else // _MSC_VER
	__attribute__((packed))
#endif
	SectionHeader
{
	/// @name Section Header
	///@{

	///Name
	char Name[8];
	///The total size of the section when loaded into memory.
	uint32_t VirtualSize;
	///For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
	uint32_t VirtualAddress;
	///Size of raw data
	uint32_t SizeOfRawData;
	///The file pointer to the first page of the section within the COFF file.
	uint32_t PointerToRawData;
	///The file pointer to the beginning of relocation entries for the section.
	uint32_t PointerToRelocations;
	///The file pointer to the beginning of line-number entries for the section.
	uint32_t PointerToLinenumbers;
	///The number of relocation entries for the section. This is set to zero for executable images.
	uint16_t NumberOfRelocations;
	///The number of line-number entries for the section.
	uint16_t NumberOfLinenumbers;
	///The flags that describe the characteristics of the section. For more information, see Section Flags.
	uint32_t Characteristics;
	///@}
};

/// Structure containing PE file data
struct
#ifdef _MSC_VER
#pragma packed(push, 1)
#else // _MSC_VER
	__attribute__((packed))
#endif
	PEFile
{
	/// @name Offsets within file
	///@{

	/// Offset to a file magic
	uint32_t MagicOffset;
	/// Offset to a main PE header
	uint32_t HeaderOffset;
	/// Offset to an optional header
	uint32_t OptionalHeaderOffset;
	/// Offset to a section table
	uint32_t SectionHeaderOffset;
	///@}

	/// @name File headers
	///@{

	/// File magic
	uint32_t Magic;
	/// Main header
	struct PEHeader Header;
	/// Optional header
	struct OptionalHeader OptionalHeader;
	/// Array of section headers with the size of header.number_of_sections
	struct SectionHeader* SectionHeader;
	///@}
};
