/// @file
/// @brief Work with section
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/// @brief Read PE file
/// @param[in] inFile The file to read
/// @param[in] peFile Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads data from file into a structure
bool readPE(FILE* inFile, struct PEFile* peFile);

/// @brief Create section headers
/// @param[in] number Number of sections
/// @return  void* Pointer to sections headers
/// @details Frees Allocate memory for sections headers
void* createSecHeaders(uint16_t number);

/// @brief Read section headers
/// @param[in] inFile The file to read
/// @param[in] peFile Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads sections headers from file into a structure
bool readSecHeaders(FILE* inFile, struct PEFile* peFile);

/// @brief Find section
/// @param[in] peFile Structure containing PE file data
/// @param[in] name Name section
/// @return  section_find Found section
/// @details Search section by name
struct SectionHeader findSection(struct PEFile* peFile, char* name);

/// @brief Save section
/// @param[in] inFile The file to read
/// @param[in] outFile The file to write
/// @param[in] sectionHeader Section for save
/// @details Writes a section to a file
void writeSection(FILE* inFile, FILE* outFile, struct SectionHeader* sectionHeader);
