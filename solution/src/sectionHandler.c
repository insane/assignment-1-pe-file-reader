/// @file
/// @brief Work with section
#include "peHeader.h"
#include "sectionHandler.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// @brief Read PE file
/// @param[in] inFile The file to read
/// @param[in] peFile Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads data from file into a structure
bool readPE(FILE* inFile, struct PEFile* peFile) {
    peFile->MagicOffset = 0x3C;
    if (fseek(inFile, peFile->MagicOffset, SEEK_SET) != 0) {
        return false;
    }
    if (fread(&peFile->HeaderOffset, sizeof(uint32_t), 1, inFile) != 1) {
        return false;
    }
    if (fseek(inFile, peFile->HeaderOffset, SEEK_SET) != 0) {
        return false;
    }

    if (fread(&peFile->Magic, sizeof(uint32_t), 1, inFile) != 1) {
        return false;
    }

    if (fread(&peFile->Header, sizeof(struct PEHeader), 1, inFile) != 1) {
        return false;
    }

    if (fread(&peFile->OptionalHeader, sizeof(struct OptionalHeader), 1, inFile) != 1) {
        return false;
    }
    return true;

}

/// @brief Create section headers
/// @param[in] number Number of sections
/// @return  void* Pointer to sections headers
/// @details Frees Allocate memory for sections headers
void* createSecHeaders(uint16_t number) {
    return malloc(sizeof(struct SectionHeader) * number);
}

/// @brief Read section headers
/// @param[in] inFile The file to read
/// @param[in] peFile Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads sections headers from file into a structure
bool readSecHeaders(FILE* inFile, struct PEFile* peFile) {
    for (uint16_t i = 0;i < peFile->Header.NumberOfSections;i++) {
        if (fread(&peFile->SectionHeader[i], sizeof(struct SectionHeader), 1, inFile) != 1) {
            return false;
        }
    }
    return true;
}



/// @brief Find section
/// @param[in] peFile Structure containing PE file data
/// @param[in] name Name section
/// @return  section_find Found section
/// @details Search section by name
struct SectionHeader findSection(struct PEFile* peFile, char* name) {
    for (uint16_t i = 0; i < peFile->Header.NumberOfSections; i++) {
        if (strcmp(peFile->SectionHeader[i].Name, name) == 0) {
            return peFile->SectionHeader[i];
        }
    }
    return (struct SectionHeader){ 0 };
}

/// @brief Save section
/// @param[in] inFile The file to read
/// @param[in] outFile The file to write
/// @param[in] sectionHeader Section for save
/// @details Writes a section to a file
void writeSection(FILE* inFile, FILE* outFile, struct SectionHeader* sectionHeader) {
    fseek(inFile, sectionHeader->PointerToRawData, SEEK_SET);
    char* outSec = malloc(sectionHeader->SizeOfRawData);
    fread(outSec, sizeof(char), sectionHeader->SizeOfRawData, inFile);
    fwrite(outSec, sizeof(char), sectionHeader->SizeOfRawData, outFile); 
    free(outSec);
}
