/// @file 
/// @file
/// @brief Main application file
#include "peHeader.h"
#include "sectionHandler.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE* f)
{
	fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
int main(int argc, char** argv)
{
	(void)argc; (void)argv; // supress 'unused parameters' warning

	usage(stdout);

	FILE* inFile = fopen(argv[1], "rb");
	struct PEFile peFile = { 0 };
	readPE(inFile, &peFile);
	peFile.SectionHeader = createSecHeaders(peFile.Header.NumberOfSections);
	readSecHeaders(inFile, &peFile);
	struct SectionHeader section = findSection(&peFile, argv[2]);
	FILE* outFile = fopen(argv[3], "wb");
	writeSection(inFile, outFile, &section);
	fclose(inFile);
	fclose(outFile);
	free(peFile.SectionHeader);
}
